# Index

This list is an attempt to collect free resources about graphics, rendering techniques, engine development, game developement, engine architecture, optimization techniques, and more. This document will grow as I discover more interesting resources.

### Rendering Techniques

- [Voxels](#voxels)
- [Water](#water)
- [Antialiasing](#antialiasing)
- [Raytracing](#raytracing)
- [Ray Marching](#ray-marching)
- [Optimization Techniques](#optimization-techniques)

### APIs

- [Vulkan](#vulkan)
    - [General Tutorials](#general-tutorials)
    - [Descriptors](#descriptors)
    - [Performance](#performance)
    - [Extensions](#extensions)


### Engine Development

- [Render Graphs](#render-graphs) 
- [Data Oriented Design](#data-oriented-design)
- [ECS](#ecs)

---

# Voxels

- [Paper] [A Ray-Box Intersection Algorithm and Efficient Dynamic Voxel Rendering](http://jcgt.org/published/0007/03/04/)
- [Blog] [Meshing in a Minecraft Game](https://0fps.net/2012/06/30/meshing-in-a-minecraft-game/)
- [Blog] [The basics of GPU voxelization](https://developer.nvidia.com/content/basics-gpu-voxelization)
- [Paper] [Real-time voxel rendering algorithm based on SSBVB and SVB - Jablonsky](https://otik.zcu.cz/bitstream/11025/29528/1/Jablonsky.pdf)
- [Paper][Efficient Sparse Voxel Octrees](https://research.nvidia.com/sites/default/files/pubs/2010-02_Efficient-Sparse-Voxel/laine2010tr1_paper.pdf)
- [Paper][A Fast Voxel Traversal Algorithm for Raytracing](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.42.3443&rep=rep1&type=pdf#:~:text=A%20fast%20and%20simple%20voxel,than%20one%20voxel%20are%20eliminated)

# Water

- [Paper] [Tessendorf - Simulating Ocean Water](https://people.cs.clemson.edu/~jtessen/reports/papers_files/coursenotes2004.pdf)
- [Paper] [NVIDIA Water Surface Wavelets](http://visualcomputing.ist.ac.at/publications/2018/WSW/)

# Antialiasing

- [Blog] [Implementing FXAA](http://blog.simonrodriguez.fr/articles/2016/07/implementing_fxaa.html)

# Raytracing

- [Book] [Raytracing in one weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html)
- [Tutorial] [NVIDIA Vulkan Ray Tracing Tutorial](https://nvpro-samples.github.io/vk_raytracing_tutorial_KHR/)

# Ray Marching

- [Blog][Ray Marching - Michael Walczyk](https://michaelwalczyk.com/blog-ray-marching.html)

# Optimization Techniques
- [Blog][Octree Textures - Nvidia](https://developer.nvidia.com/gpugems/gpugems2/part-v-image-oriented-computing/chapter-37-octree-textures-gpu)

# Vulkan

### General Tutorials

- [Tutorial] [Vulkan Tutorial](https://vulkan-tutorial.com/)

### Descriptors

- [Blog] [Bindless Descriptors](https://wickedengine.net/2021/04/06/bindless-descriptors/)
- [Blog] [Moving The Machinery to Bindless](https://ourmachinery.com/post/moving-the-machinery-to-bindless/)
- [Blog] [Binding Bindlessly](https://alextardif.com/Bindless.html)

### Performance

- [Blog] [Writing an efficient Vulkan renderer](https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/)

### Extensions

- [Blog][Vulkan sparse bindings](https://www.asawicki.info/news_1698_vulkan_sparse_binding_-_a_quick_overview)

# Engine Development

### Render Graphs

- [Blog] [Render graphs and Vulkan](http://themaister.net/blog/2017/08/15/render-graphs-and-vulkan-a-deep-dive/)
- [Blog] [Render graph optimization scribbes](https://www.jeremyong.com/rendering/2019/06/28/render-graph-optimization-scribbles/)
- [Blog] [Render graph - Simon's Tech Blog](https://simonstechblog.blogspot.com/2019/07/render-graph.html)
- [Slides] [FrameGraph](https://www.slideshare.net/DICEStudio/framegraph-extensible-rendering-architecture-in-frostbite) 

### Data Oriented Design

- [Talk] [Mike Acton - Data Oriented Design](https://www.youtube.com/watch?v=rX0ItVEVjHc)

### ECS

- [Blog] [Tobias Stein - Entity Component System](https://www.gamedeveloper.com/design/the-entity-component-system---an-awesome-game-design-pattern-in-c-part-1-)
